# Snake

> My implementation of Snake in MATLAB, for ENG 1002.

## Introduction

This version of Snake includes multiplayer and custom maps.

I built it as part of my ENG 1002 course.

## Usage

Using [MATLAB](https://mathworks.com/products/matlab.html), open the the [Project File](Snake.prj).

Next, run the [main](./src/main.m) method.

```matlab
main
```

It is possible to customise the step rate. The default is `0.25`.

```matlab
main(0.1)
```

To run the test suite, use the [runtests](https://mathworks.com/help/matlab/ref/runtests.html) method.

```matlab
runtests("tests", "IncludeSubfolders", true)
```

## License

[MIT](LICENSE)
