%% Main

function tests = mustBeDirectionTest
    tests = functiontests(localfunctions);
end

%% mustBeDirection()

function mustBeDirectionArgumentsTest(testCase)
    % 0
    handle = @() mustBeDirection();

    assertError(testCase, handle, "MATLAB:minrhs");

    % 1
    assertWarningFree(testCase, @() mustBeDirection(Direction.Up));

    % 2+
    handle = @() mustBeDirection(1, 1);

    assertError(testCase, handle, "MATLAB:TooManyInputs");
end

function mustBeDirectionValueDirectionTest(testCase)
    assertWarningFree(testCase, @() mustBeDirection(Direction.Up));
    assertWarningFree(testCase, @() mustBeDirection(Direction.Down));
    assertWarningFree(testCase, @() mustBeDirection(Direction.Left));
    assertWarningFree(testCase, @() mustBeDirection(Direction.Right));
end

function mustBeDirectionValueInvalidTest(testCase)
    eID = "SNAKE:validators:mustBeDirection";

    assertError(testCase, @() mustBeDirection(Direction.Up * 2), eID);
    assertError(testCase, @() mustBeDirection(int8([1, 1])), eID);
    assertError(testCase, @() mustBeDirection(5), eID);
    assertError(testCase, @() mustBeDirection("Left"), eID);
    assertError(testCase, @() mustBeDirection(@() Direction.Right), eID);
end
