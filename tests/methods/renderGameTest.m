%% Main

function tests = renderGameTest
    tests = functiontests(localfunctions);
end

%% renderGame()

function renderGameArgumentsTest(testCase)
    % 0
    handle = @() renderGame();

    assertError(testCase, handle, "MATLAB:minrhs");

    % 1
    assertWarningFree(testCase, @() renderGame(SnakeGame()));

    % 2+
    handle = @() renderGame(SnakeGame(), 1);

    assertError(testCase, handle, "MATLAB:TooManyInputs");
end

function renderGameSnakeGameInvalidTypeTest(testCase)
    % String
    handle = @() renderGame("InvalidType");

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Cell Array
    handle = @() renderGame({});

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Struct
    handle = @() renderGame(struct("name", "John"));

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");
end

function renderGameSnakeGameInvalidSizeTest(testCase)
    % Missing Column
    handle = @() renderGame([SnakeGame(), SnakeGame()]);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");

    % Extra Column
    handle = @() renderGame([SnakeGame(); SnakeGame()]);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");
end

function renderGameImageTest(testCase)
    % Map Listing
    mapListing = dir("../../data/maps/*.json");

    % Iterate over Listing
    for i = 1:length(mapListing)
        % Load Map
        mapPath = [mapListing(i).folder, '/', mapListing(i).name];
        map = Map.loadJson(mapPath);

        % Determine Result
        expResult = imread(['./renderGame/0', int2str(i), '.png']);
        actResult = renderGame(SnakeGame(map));

        % Check
        assertEqual(testCase, expResult, actResult);
    end
end
