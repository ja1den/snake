%% Main

function tests = mapTest
    tests = functiontests(localfunctions);
end

%% Map.Map()

function mapArgumentsTest(testCase)
    % 0
    map = Map();

    assertEqual(testCase, map.Name, "Empty");
    assertEqual(testCase, map.Blocks, zeros(0, 4, "int8"));

    % 1
    handle = @() Map("Map 1");

    assertError(testCase, handle, "MATLAB:minrhs");

    % 2 (String)
    map = Map("Map 1", [1, 1, 2, 2]);

    assertEqual(testCase, map.Name, "Map 1");
    assertEqual(testCase, map.Blocks, int8([1, 1, 2, 2]));

    % 2 (Characters)
    map = Map('Map 1', [1, 1, 2, 2]);

    assertEqual(testCase, map.Name, "Map 1");
    assertEqual(testCase, map.Blocks, int8([1, 1, 2, 2]));

    % 3+
    handle = @() Map("Map 1", [1, 1, 2, 2], 5);

    assertError(testCase, handle, "MATLAB:TooManyInputs");
end

function mapNameInvalidTypeTest(testCase)
    handle = @() Map(struct("name", "John"), [1, 1, 2, 2]);

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");
end

function mapNameInvalidSizeTest(testCase)
    % Two Strings
    handle = @() Map(["abc", "defg"], [1, 1, 1, 1]);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");

    % Cell Array
    handle = @() Map({}, [1, 1, 2, 2]);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");
end

function mapNameInvalidRangeTest(testCase)
    % String
    handle = @() Map("", [1, 1, 1, 1]);

    assertError(testCase, handle, "MATLAB:validators:" + ...
        "mustBeNonzeroLengthText");

    % Characters
    handle = @() Map('', [1, 1, 1, 1]);

    assertError(testCase, handle, "MATLAB:validators:" + ...
        "mustBeNonzeroLengthText");
end

function mapBlocksInvalidTypeTest(testCase)
    % String
    handle = @() Map("Map 1", "InvalidType");

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Cell Array
    handle = @() Map("Map 1", {});

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Struct
    handle = @() Map("Map 1", struct("name", "John"));

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");
end

function mapBlocksInvalidSizeTest(testCase)
    % Missing Column
    handle = @() Map("Map 1", [10, 10, 15]);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");

    % Extra Column
    handle = @() Map("Map 1", [10, 10, 15, 15, 20]);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");
end

function mapBlocksInvalidRangeTest(testCase)
    % Negative
    handle = @() Map("Map 1", [-3, 5, -5, 17]);

    assertError(testCase, handle, "MATLAB:validators:mustBeInRange");

    % Zero
    handle = @() Map("Map 1", [0, 5, 21, 25]);

    assertError(testCase, handle, "MATLAB:validators:mustBeInRange");

    % Positive
    handle = @() Map("Map 1", [50, 50, 75, 75]);

    assertError(testCase, handle, "MATLAB:validators:mustBeInRange");
end

%% Map.loadJson()

function loadJsonArgumentsTest(testCase)
    % 0
    handle = @() Map.loadJson();

    assertError(testCase, handle, "MATLAB:minrhs");

    % 1+
    handle = @() Map.loadJson("01.json", "02.json");

    assertError(testCase, handle, "MATLAB:TooManyInputs");
end

function loadJsonPathInvalidTypeTest(testCase)
    handle = @() Map.loadJson(struct("name", "John"));

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");
end

function loadJsonPathInvalidSizeTest(testCase)
    % Two Strings
    handle = @() Map.loadJson(["abc", "defg"]);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");

    % Cell Array
    handle = @() Map.loadJson({});

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");
end

function loadJsonPathInvalidPathTest(testCase)
    handle = @() Map.loadJson("map/00.json");

    assertError(testCase, handle, "MATLAB:validators:mustBeFile");
end

function loadJsonPathInvalidFileTest(testCase)
    % Name - Empty
    handle = @() Map.loadJson("map/01.json");

    assertError(testCase, handle, "MATLAB:nonExistentField");

    % Name - Invalid Type
    handle = @() Map.loadJson("map/02.json");

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Name - Invalid Size
    handle = @() Map.loadJson("map/03.json");

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");

    % Name - Invalid Range
    handle = @() Map.loadJson("map/04.json");

    assertError(testCase, handle, "MATLAB:validators:" + ...
        "mustBeNonzeroLengthText");

    % Blocks - Empty
    handle = @() Map.loadJson("map/05.json");

    assertError(testCase, handle, "MATLAB:nonExistentField");

    % Blocks - Invalid Type
    handle = @() Map.loadJson("map/06.json");

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Blocks - Invalid Size
    handle = @() Map.loadJson("map/07.json");

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");

    % Blocks - Invalid Range (Negative)
    handle = @() Map.loadJson("map/08.json");

    assertError(testCase, handle, "MATLAB:validators:mustBeInRange");

    % Blocks - Invalid Range (Zero)
    handle = @() Map.loadJson("map/09.json");

    assertError(testCase, handle, "MATLAB:validators:mustBeInRange");

    % Blocks - Invalid Range (Positive)
    handle = @() Map.loadJson("map/10.json");

    assertError(testCase, handle, "MATLAB:validators:mustBeInRange");
end

%% Map.collide()

function collideArgumentsTest(testCase)
    map = Map("Map 1", [11, 15, 11, 15]);

    % 0
    handle = @() map.collide();

    assertError(testCase, handle, "MATLAB:minrhs");

    % 1
    assertTrue(testCase, map.collide([15, 15]));

    % 2+
    handle = @() map.collide([1, 1], [2, 2]);

    assertError(testCase, handle, "MATLAB:TooManyInputs");
end

function collidePointInvalidTypeTest(testCase)
    map = Map("Map 1", [11, 15, 11, 15]);

    % String
    handle = @() map.collide("InvalidType");

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Cell Array
    handle = @() map.collide({});

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Struct
    handle = @() map.collide(struct("name", "John"));

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");
end

function collidePointInvalidSizeTest(testCase)
    map = Map("Map 1", [11, 15, 11, 15]);

    % Extra Column
    handle = @() map.collide([1, 2, 3]);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");

    % Extra Row
    handle = @() map.collide([1, 2; 3, 4]);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");
end

function collidePointInvalidRangeTest(testCase)
    map = Map("Map 1", [11, 15, 11, 15]);

    % Negative
    handle = @() map.collide([-3, 30]);

    assertError(testCase, handle, "MATLAB:validators:mustBeInRange");

    % Zero
    handle = @() map.collide(0);

    assertError(testCase, handle, "MATLAB:validators:mustBeInRange");

    % Positive
    handle = @() map.collide([30, 75]);

    assertError(testCase, handle, "MATLAB:validators:mustBeInRange");
end

function collideHitMissTest(testCase)
    map = Map("Map 1", [11, 15, 11, 15]);

    % Hit
    assertTrue(testCase, map.collide([11, 11]));
    assertTrue(testCase, map.collide([11, 15]));
    assertTrue(testCase, map.collide([15, 11]));
    assertTrue(testCase, map.collide([15, 15]));

    % Miss
    assertFalse(testCase, map.collide([10, 10]));
    assertFalse(testCase, map.collide([10, 16]));
    assertFalse(testCase, map.collide([16, 10]));
    assertFalse(testCase, map.collide([16, 16]));
end
