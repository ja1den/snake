%% Main

function tests = snakeGameTest
    tests = functiontests(localfunctions);
end

%% SnakeGame.SnakeGame()

function snakeGameArgumentsTest(testCase)
    % 0
    snakeGame = SnakeGame();

    assertEqual(testCase, snakeGame.Map, Map());
    assertEqual(testCase, length(snakeGame.Snakes), 2);
    assertEqual(testCase, snakeGame.Losers, [false, false]);

    % 1
    snakeGame = SnakeGame(Map("Map 1", [11, 15, 11, 15]));

    assertEqual(testCase, snakeGame.Map.Name, "Map 1");
    assertEqual(testCase, snakeGame.Map.Blocks, int8([11, 15, 11, 15]));
    assertEqual(testCase, length(snakeGame.Snakes), 2);
    assertEqual(testCase, snakeGame.Losers, [false, false]);

    % 2+
    handle = @() SnakeGame(Map("Map 1", [11, 15, 11, 15]), 1024);

    assertError(testCase, handle, "MATLAB:TooManyInputs");
end

function snakeGameMapInvalidTypeTest(testCase)
    % String
    handle = @() SnakeGame("InvalidType");

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Cell Array
    handle = @() SnakeGame({});

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Struct
    handle = @() SnakeGame(struct("name", "John"));

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");
end

function snakeGameMapInvalidSizeTest(testCase)
    handle = @() SnakeGame([Map(), Map(), Map()]);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");
end

%% SnakeGame.step()

function stepArgumentsTest(testCase)
    snakeGame = SnakeGame(Map("Map 1", [11, 15, 11, 15]));

    % 0
    handle = @() snakeGame.step();

    assertWarningFree(testCase, handle);

    % 1+
    handle = @() snakeGame.step(5);

    assertError(testCase, handle, "MATLAB:TooManyInputs");
end

function stepPositionTest(testCase)
    snakeGame = SnakeGame();

    assertEqual(testCase, snakeGame.Snakes(1).Cells(1, :), ...
        int8([5, 5]));
    assertEqual(testCase, snakeGame.Snakes(2).Cells(1, :), ...
        int8([46, 46]));

    % Forwards
    snakeGame = snakeGame.step();

    assertEqual(testCase, snakeGame.Snakes(1).Cells(1, :), ...
        int8([6, 5]));
    assertEqual(testCase, snakeGame.Snakes(2).Cells(1, :), ...
        int8([45, 46]));

    % Turn
    snakeGame.Snakes(1) = ...
        snakeGame.Snakes(1).setDirection(Direction.Right);
    snakeGame.Snakes(2) = ...
        snakeGame.Snakes(2).setDirection(Direction.Left);

    snakeGame = snakeGame.step();

    assertEqual(testCase, snakeGame.Snakes(1).Cells(1, :), ...
        int8([6, 6]));
    assertEqual(testCase, snakeGame.Snakes(2).Cells(1, :), ...
        int8([45, 45]));
end

function stepSizeTest(testCase)
    snakeGame = SnakeGame();

    % Check 20 Steps
    for i = 1:20
        % Determine Result
        if i < 3
            expResult = [i, 2];
        else
            expResult = [int8(i) / 6 + 2, 2];
        end

        % Check
        assertSize(testCase, snakeGame.Snakes(1).Cells, expResult);
        assertSize(testCase, snakeGame.Snakes(2).Cells, expResult);

        % Step
        snakeGame = snakeGame.step();
    end
end

function stepCollisionTest(testCase)
    snakeGame = SnakeGame(Map("Map 1", [7, 7, 5, 5]));

    assertEqual(testCase, snakeGame.Step, int32(1));
    assertEqual(testCase, snakeGame.Losers, [false, false]);

    snakeGame = snakeGame.step();

    assertEqual(testCase, snakeGame.Step, int32(2));
    assertEqual(testCase, snakeGame.Losers, [false, false]);

    snakeGame = snakeGame.step();

    assertEqual(testCase, snakeGame.Step, int32(3));
    assertEqual(testCase, snakeGame.Losers, [true, false]);

    snakeGame = snakeGame.step();

    assertEqual(testCase, snakeGame.Step, int32(3));
end
