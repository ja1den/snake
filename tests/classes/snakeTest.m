%% Main

function tests = snakeTest
    tests = functiontests(localfunctions);
end

%% Snake.Snake()

function snakeArgumentsTest(testCase)
    % 0
    snake = Snake();

    assertEqual(testCase, snake.Cells, int8([1, 1]));
    assertEqual(testCase, snake.Direction, Direction.Up);
    assertEqual(testCase, snake.Step, int32(1));

    % 1
    handle = @() Snake([5, 5]);

    assertError(testCase, handle, "MATLAB:minrhs");

    % 2 (Tuple)
    snake = Snake([3, 4], Direction.Down);

    assertEqual(testCase, snake.Cells, int8([3, 4]));
    assertEqual(testCase, snake.Direction, Direction.Down);
    assertEqual(testCase, snake.Step, int32(1));

    % 2 (Scalar)
    snake = Snake(5, Direction.Left);

    assertEqual(testCase, snake.Cells, int8([5, 5]));
    assertEqual(testCase, snake.Direction, Direction.Left);
    assertEqual(testCase, snake.Step, int32(1));

    % 3+
    handle = @() Snake([5, 5], Direction.Right, 13);

    assertError(testCase, handle, "MATLAB:TooManyInputs");
end

function snakeCellsInvalidTypeTest(testCase)
    % String
    handle = @() Snake("InvalidType", Direction.Up);

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Cell Array
    handle = @() Snake({}, Direction.Up);

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Struct
    handle = @() Snake(struct("name", "John"), Direction.Up);

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");
end

function snakeCellsInvalidSizeTest(testCase)
    handle = @() Snake([1, 2, 3; 4, 5, 6], Direction.Up);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");
end

function snakeCellsInvalidRangeTest(testCase)
    % Negative
    handle = @() Snake([-3, 30], Direction.Up);

    assertError(testCase, handle, "MATLAB:validators:mustBeInRange");

    % Zero
    handle = @() Snake(0, Direction.Up);

    assertError(testCase, handle, "MATLAB:validators:mustBeInRange");

    % Positive
    handle = @() Snake([30, 75], Direction.Up);

    assertError(testCase, handle, "MATLAB:validators:mustBeInRange");
end

function snakeDirectionInvalidTypeTest(testCase)
    % String
    handle = @() Snake([5, 5], "NaN");

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Cell Array
    handle = @() Snake([5, 5], {});

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Struct
    handle = @() Snake([5, 5], struct("name", "John"));

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");
end

function snakeDirectionInvalidSizeTest(testCase)
    % Extra Column
    handle = @() Snake([5, 5], [1, 2, 3]);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");

    % Extra Row
    handle = @() Snake([5, 5], [1, 2; 3, 4]);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");
end

function snakeDirectionInvalidRangeTest(testCase)
    % Empty
    handle = @() Snake([5, 5], int8([0, 0]));

    assertError(testCase, handle, "SNAKE:validators:mustBeDirection");

    % Double Length
    handle = @() Snake([5, 5], Direction.Up * 2);

    assertError(testCase, handle, "SNAKE:validators:mustBeDirection");

    % Not Cardinal
    handle = @() Snake([5, 5], [2, 3]);

    assertError(testCase, handle, "SNAKE:validators:mustBeDirection");
end

%% Snake.setDirection()

function setDirectionArgumentsTest(testCase)
    snake = Snake([5, 5], Direction.Up);

    % 0
    handle = @() snake.setDirection();

    assertError(testCase, handle, "MATLAB:minrhs");

    % 1
    snake = snake.setDirection(Direction.Right);

    assertEqual(testCase, snake.Direction, Direction.Right);

    % 2+
    handle = @() snake.setDirection(Direction.Right, Direction.Right);

    assertError(testCase, handle, "MATLAB:TooManyInputs");
end

function setDirectionDirectionInvalidTypeTest(testCase)
    snake = Snake([5, 5], Direction.Up);

    % String
    handle = @() snake.setDirection("NaN");

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Cell Array
    handle = @() snake.setDirection({});

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Struct
    handle = @() snake.setDirection(struct("name", "John"));

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");
end

function setDirectionDirectionInvalidSizeTest(testCase)
    snake = Snake([5, 5], Direction.Up);

    % Extra Column
    handle = @() snake.setDirection([1, 2, 3]);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");

    % Extra Row
    handle = @() snake.setDirection([1, 2; 3, 4]);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");
end

function setDirectionDirectionInvalidRangeTest(testCase)
    snake = Snake([5, 5], Direction.Up);

    % Empty
    handle = @() snake.setDirection(int8([0, 0]));

    assertError(testCase, handle, "SNAKE:validators:mustBeDirection");

    % Double Length
    handle = @() snake.setDirection(Direction.Up * 2);

    assertError(testCase, handle, "SNAKE:validators:mustBeDirection");

    % Not Cardinal
    handle = @() snake.setDirection([2, 3]);

    assertError(testCase, handle, "SNAKE:validators:mustBeDirection");
end

function setDirectionReversalTest(testCase)
    snake = Snake([5, 5], Direction.Up);

    snake = snake.step();

    % Simple Reversal
    snake = snake.setDirection(Direction.Down);

    assertNotEqual(testCase, snake.Direction, Direction.Down);

    % Complex Reversal
    snake = snake.setDirection(Direction.Left);
    snake = snake.setDirection(Direction.Down);

    assertNotEqual(testCase, snake.Direction, Direction.Down);

    % Correct Reversal
    snake = snake.setDirection(Direction.Left);
    snake = snake.step();
    snake = snake.setDirection(Direction.Down);

    assertEqual(testCase, snake.Direction, Direction.Down);
end

%% Snake.step()

function stepArgumentsTest(testCase)
    snake = Snake([5, 5], Direction.Up);

    % 0
    handle = @() snake.step();

    assertWarningFree(testCase, handle);

    % 1+
    handle = @() snake.step(5);

    assertError(testCase, handle, "MATLAB:TooManyInputs");
end

function stepPositionTest(testCase)
    snake = Snake([5, 5], Direction.Down);

    assertEqual(testCase, snake.Cells(1, :), int8([5, 5]));

    % Move Down
    snake = snake.step();

    assertEqual(testCase, snake.Cells(1, :), int8([6, 5]));

    % Move Left
    snake = snake.setDirection(Direction.Left);
    snake = snake.step();

    assertEqual(testCase, snake.Cells(1, :), int8([6, 4]));

    % Move Up
    snake = snake.setDirection(Direction.Up);
    snake = snake.step();

    assertEqual(testCase, snake.Cells(1, :), int8([5, 4]));

    % Move Down
    snake = snake.setDirection(Direction.Down);
    snake = snake.step();

    assertEqual(testCase, snake.Cells(1, :), int8([4, 4]));
end

function stepSizeTest(testCase)
    snake = Snake([5, 5], Direction.Down);

    % Check 20 Steps
    for i = 1:20
        if i < 3
            assertSize(testCase, snake.Cells, [i, 2]);
        else
            assertSize(testCase, snake.Cells, [int8(i) / 6 + 2, 2]);
        end

        snake = snake.step();
    end
end

%% Snake.collide()

function collideArgumentsTest(testCase)
    snake = Snake([5, 5], Direction.Right);

    % 0
    handle = @() snake.collide();

    assertError(testCase, handle, "MATLAB:minrhs");

    % 1
    assertTrue(testCase, snake.collide([5, 5]));

    % 2
    assertTrue(testCase, snake.collide([5, 5], false));

    % 3+
    handle = @() snake.collide([1, 1], 0, 13);

    assertError(testCase, handle, "MATLAB:TooManyInputs");
end

function collidePointInvalidTypeTest(testCase)
    snake = Snake([5, 5], Direction.Up);

    % String
    handle = @() snake.collide("InvalidType");

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Cell Array
    handle = @() snake.collide({});

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Struct
    handle = @() snake.collide(struct("name", "John"));

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");
end

function collidePointInvalidSizeTest(testCase)
    snake = Snake([5, 5], Direction.Up);

    % Extra Column
    handle = @() snake.collide([1, 2, 3]);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");

    % Extra Row
    handle = @() snake.collide([1, 2; 3, 4]);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");
end

function collidePointInvalidRangeTest(testCase)
    snake = Snake([5, 5], Direction.Up);

    % Negative
    handle = @() snake.collide([-3, 30]);

    assertError(testCase, handle, "MATLAB:validators:mustBeInRange");

    % Zero
    handle = @() snake.collide(0);

    assertError(testCase, handle, "MATLAB:validators:mustBeInRange");

    % Positive
    handle = @() snake.collide([30, 75]);

    assertError(testCase, handle, "MATLAB:validators:mustBeInRange");
end

function collideIgnoreHeadInvalidTypeTest(testCase)
    snake = Snake([5, 5], Direction.Up);

    % String
    handle = @() snake.collide([5, 5], "InvalidType");

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Cell Array
    handle = @() snake.collide([5, 5], {});

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");

    % Struct
    handle = @() snake.collide([5, 5], struct("name", "John"));

    assertError(testCase, handle, "MATLAB:validation:UnableToConvert");
end

function collideIgnoreHeadInvalidSizeTest(testCase)
    snake = Snake([5, 5], Direction.Up);

    % Extra Column
    handle = @() snake.collide([5, 5], [false, false]);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");

    % Extra Row
    handle = @() snake.collide([5, 5], [false; false]);

    assertError(testCase, handle, "MATLAB:validation:IncompatibleSize");
end

function collideHitMissTest(testCase)
    snake = Snake([5, 5], Direction.Right);

    snake = snake.step();
    snake = snake.step();
    snake = snake.step();

    % Hit
    assertTrue(testCase, snake.collide([5, 6]));
    assertTrue(testCase, snake.collide([5, 7]));
    assertTrue(testCase, snake.collide([5, 8]));

    % Miss
    assertFalse(testCase, snake.collide([5, 5]));
    assertFalse(testCase, snake.collide([5, 9]));
    assertFalse(testCase, snake.collide([4, 5]));
    assertFalse(testCase, snake.collide([4, 7]));
end
