function main(stepRate)
    % MAIN The main method for Snake.
    %   MAIN(R) starts a new game of snake with step rate R.
    %
    %   See also: SnakeGame, State

    arguments
        stepRate double = 0.25;
    end

    %% Initialisation

    % Map Listing
    mapListing = dir("./data/maps/*.json");

    % Preallocate Maps Array
    maps = repmat(Map(), length(mapListing), 1);

    % Iterate over Listing
    for i = 1:length(mapListing)
        % Load Map
        mapPath = [mapListing(i).folder, '/', mapListing(i).name];
        maps(i) = Map.loadJson(mapPath);
    end

    % Create SnakeGame
    snakeGame = SnakeGame(maps(1));

    % Create UI
    interface = UI(arrayfun(@(map) map.Name, maps));

    interface.Figure.KeyPressFcn = @handleFigureKeyPress;
    interface.Figure.DeleteFcn = @handleFigureDelete;

    interface.MapPopup.Callback = @handleMapPopup;
    interface.PlayButton.Callback = @handlePlayButton;

    %% Main

    % Global Loop
    while isvalid(interface.Figure)
        % State
        state = State.Menu;

        % Render Board
        imshow(renderGame(snakeGame), "Parent", interface.Board);

        % Freeze UI
        uiwait(interface.Figure);

        % Game Loop
        while state == State.Playing
            % Pause
            pause(stepRate);

            % Check UI
            if ~isvalid(interface.Figure); return; end

            % Step the Game
            snakeGame = snakeGame.step();

            % Check Losers
            if sum(snakeGame.Losers) ~= 0
                state = State.Ended;
            end

            % Render Board
            imshow(renderGame(snakeGame), "Parent", interface.Board);
        end

        % Check UI
        if ~isvalid(interface.Figure); return; end

        % Enable Interface
        interface.MapPopup.Enable = "on";
        interface.PlayButton.Enable = "on";

        % Display Popup
        if sum(snakeGame.Losers) == 2
            interface.setPopupText("Draw!", 24);
        elseif snakeGame.Losers(1)
            interface.setPopupText("Player 2 wins!", 24);
        else
            interface.setPopupText("Player 1 wins!", 24);
        end
    end

    %% Event Handlers

    % Map Select
    function handleMapPopup(src, ~)
        % Create SnakeGame
        snakeGame = SnakeGame(maps(src.Value));

        % Hide Popup
        interface.setPopupText();

        % Render Board
        imshow(renderGame(snakeGame), "Parent", interface.Board);
    end

    % Play Button
    function handlePlayButton(~, ~)
        % Create SnakeGame
        snakeGame = SnakeGame(snakeGame.Map);

        % Hide Popup
        interface.setPopupText();

        % Render Board
        imshow(renderGame(snakeGame), "Parent", interface.Board);

        % Disable Interface
        interface.MapPopup.Enable = "off";
        interface.PlayButton.Enable = "off";

        % Count Down
        for j = 3:-1:1
            % Check Figure
            if isvalid(interface.Figure)
                % Set Text
                interface.setPopupText(num2str(j), 48);
            else
                return;
            end

            pause(1);
        end

        if ~isvalid(interface.Figure); return; end

        % Hide Popup
        interface.setPopupText("");

        % State
        state = State.Playing;

        % Resume UI
        uiresume(interface.Figure);
    end

    % Figure Delete
    function handleFigureDelete(~, ~)
        % Update State
        state = State.Ended;
    end

    % Figure Key Press
    function handleFigureKeyPress(~, keyData)
        % Playing?
        if state == State.Playing
            switch keyData.Key
                % Player 1
                case "w"
                    snakeGame.Snakes(1) = ...
                        snakeGame.Snakes(1).setDirection(Direction.Up);
                case "a"
                    snakeGame.Snakes(1) = ...
                        snakeGame.Snakes(1).setDirection(Direction.Left);
                case "s"
                    snakeGame.Snakes(1) = ...
                        snakeGame.Snakes(1).setDirection(Direction.Down);
                case "d"
                    snakeGame.Snakes(1) = ...
                        snakeGame.Snakes(1).setDirection(Direction.Right);

                % Player 2
                case "uparrow"
                    snakeGame.Snakes(2) = ...
                        snakeGame.Snakes(2).setDirection(Direction.Up);
                case "leftarrow"
                    snakeGame.Snakes(2) = ...
                        snakeGame.Snakes(2).setDirection(Direction.Left);
                case "downarrow"
                    snakeGame.Snakes(2) = ...
                        snakeGame.Snakes(2).setDirection(Direction.Down);
                case "rightarrow"
                    snakeGame.Snakes(2) = ...
                        snakeGame.Snakes(2).setDirection(Direction.Right);

                % No Player
                otherwise
                    % Ignore Input
            end
        end
    end
end
