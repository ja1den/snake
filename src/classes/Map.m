classdef Map
    % MAP The core Map class.
    %   The MAP class holds a Map's name and blocks.
    %   It contains a helper method for loading Map data from JSON.
    %
    %   See also: SnakeGame

    properties (SetAccess=immutable)
        Name (1, 1) string {mustBeNonzeroLengthText} = "Empty";
        Blocks (:, 4) int8 {mustBeInRange(Blocks, 1, 50)} = [];
    end

    methods
        function obj = Map(name, blocks)
            % MAP Create a new Map.
            %   M = MAP(N, B) creates a new Map with name N and blocks B.
            %
            %   Block coordinates are expected in the order R1, R2, C1,
            %   C2, where R1 <= R2, and C1 <= C2.

            if nargin > 0
                obj.Name = name;
                obj.Blocks = blocks;
            end
        end

        function isHit = collide(obj, point)
            % COLLIDE Check a collision with the Map.
            %   H = COLLIDE(P) returns a logical as to whether point
            %   (P, P) collides with the Map.
            %
            %   H = COLLIDE([R, C]) returns a logical as to whether point
            %   (R, C) collides with the Map.

            arguments
                obj (1, 1) Map
                point (1, 2) int8 {mustBeInRange(point, 1, 50)}
            end

            % Default
            isHit = false;

            % Iterate over Blocks
            for i = 1:size(obj.Blocks, 1)
                % Get Block
                block = obj.Blocks(i, :);

                % Check Hit
                inRow = block(1) <= point(1) && point(1) <= block(2);
                inColumn = block(3) <= point(2) && point(2) <= block(4);

                if inRow && inColumn
                    isHit = true;
                end
            end
        end
    end

    methods (Static)
        function obj = loadJson(path)
            % LOADJSON Create a Map from a JSON file.
            %   M = LOADJSON(P) creates a new Map using the data loaded
            %   from the JSON file at path P.

            arguments
                path (1, 1) string {mustBeFile}
            end

            % Decode JSON
            jsonData = jsondecode(fileread(path));

            % Create a new Map
            obj = Map(jsonData.name, jsonData.blocks);
        end
    end
end
