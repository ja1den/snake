classdef Snake
    % SNAKE The core Snake class.
    %   The SNAKE class holds a Snake's cells, direction, and step.
    %   It contains methods for updating the Snake.
    %
    %   See also: SnakeGame, Direction

    properties (SetAccess=private)
        Cells (:, 2) int8 {mustBeInRange(Cells, 1, 50)} = 1;
        Direction (1, 2) int8 {mustBeDirection} = Direction.Up;
        Step (1, 1) int32 {mustBePositive} = 1;
    end

    methods
        function obj = Snake(position, direction)
            % SNAKE Create a new Snake.
            %   S = SNAKE(P, D) creates a new Snake with its head at
            %   coordinates (P, P), facing direction D.
            %
            %   S = SNAKE([R, C], D) creates a new Snake with its head at
            %   coordinates (R, C), facing direction D.

            if nargin > 0
                obj.Cells = position;
                obj.Direction = direction;
            end
        end

        function obj = setDirection(obj, direction)
            % SETDIRECTION Set the Snake's direction.
            %   S = SETDIRECTION(D) returns a copy of the Snake object
            %   with its direction set to D.

            arguments
                obj (1, 1) Snake
                direction (1, 2) int8 {mustBeDirection}
            end

            % Ignore Reversal
            if size(obj.Cells, 1) > 1
                if isequal(obj.Cells(2, :), obj.Cells(1, :) + direction)
                    return;
                end
            end

            % Set Direction
            obj.Direction = direction;
        end

        function obj = step(obj)
            % STEP Step the Snake forward.
            %   S = STEP() returns a new Snake object reflecting the
            %   current Snake object after one step.

            % Update Head (Insert)
            obj.Cells = [obj.Cells(1, :) + obj.Direction; obj.Cells];

            % Remove Tail (Delete)
            if obj.Step >= 3
                if mod(obj.Step - 2, 6) ~= 0
                    obj.Cells(size(obj.Cells, 1), :) = [];
                end
            end

            % Increment Step
            obj.Step = obj.Step + 1;
        end

        function isHit = collide(obj, point, ignoreHead)
            % COLLIDE Check a collision with the Snake.
            %   H = COLLIDE(P) returns a logical as to whether point
            %   (P, P) collides with the Snake.
            %
            %   H = COLLIDE([R, C]) returns a logical as to whether point
            %   (R, C) collides with the Snake.
            %
            %   H = COLLIDE(..., I) if the ignoreHead flag I is logical
            %   true, COLLIDE skips checking the Snake's head.

            arguments
                obj (1, 1) Snake
                point (1, 2) int8 {mustBeInRange(point, 1, 50)}
                ignoreHead (1, 1) logical = false;
            end

            % Default
            isHit = false;

            % Iterate over Cells
            for i = 1:size(obj.Cells, 1)
                % Ignore Head?
                if ignoreHead && i == 1
                    continue;
                end

                % Get Cell
                cell = obj.Cells(i, :);

                % Check Hit
                if isequal(cell, point)
                    isHit = true;
                end
            end
        end
    end
end
