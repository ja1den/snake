classdef State
    % STATE Enumeration class containing possible game states.

    enumeration
        Menu
        Playing
        Ended
    end
end
