classdef Direction
    % DIRECTION "Enumeration" class containing direction definitions.
    %
    %   See also: Snake

    properties (Constant)
        Up    = int8([-1,  0]);
        Down  = int8([ 1,  0]);
        Left  = int8([ 0, -1]);
        Right = int8([ 0,  1]);
    end
end
