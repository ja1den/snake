classdef SnakeGame
    % SNAKEGAME The core SnakeGame class.
    %   The SNAKEGAME class holds a SnakeGame's Map, Snakes, and losers.
    %   It contains methods for updating the SnakeGame.
    %
    %   See also: Map, Snake

    properties (SetAccess=immutable)
        Map (1, 1) Map = Map();
    end

    properties
        Snakes (1, 2) Snake = [Snake([5, 5], Direction.Down), ...
            Snake([46, 46], Direction.Up)];
    end

    properties (SetAccess=private)
        Step (1, 1) int32 = 1;
        Losers (1, 2) logical = [false, false];
    end

    methods
        function obj = SnakeGame(map)
            % SNAKEGAME Create a new SnakeGame.
            %   G = SNAKEGAME(M) creates a new SnakeGame with map M.

            if nargin > 0
                obj.Map = map;
            end
        end

        function obj = step(obj)
            % STEP Step the SnakeGame forward.
            %   G = STEP() returns a new SnakeGame object reflecting the
            %   current SnakeGame object after one step.

            % Check Losers
            if sum(obj.Losers) ~= 0
                return;
            end

            % Iterate over Snakes
            for i = 1:length(obj.Snakes)
                try
                    % Step
                    obj.Snakes(i) = obj.Snakes(i).step();
                catch E
                    % Hit Edge?
                    if E.identifier == "MATLAB:validators:" + ...
                            "mustBeInRange"
                        % Set Losers
                        obj.Losers(i) = true;
                    else
                        rethrow(E);
                    end
                end
            end

            % Iterate over Snakes
            for i = 1:length(obj.Snakes)
                % Get Snake
                snake = obj.Snakes(i);

                % Check Map
                if obj.Map.collide(snake.Cells(1, :))
                    % Set Losers
                    obj.Losers(i) = true;
                end

                % Check Snakes
                for j = 1:length(obj.Snakes)
                    % Ignore Head
                    ignoreHead = i == j;

                    % Check Hit
                    if obj.Snakes(j).collide(snake.Cells(1, :), ignoreHead)
                        % Set Losers
                        obj.Losers(i) = true;
                    end
                end
            end

            % Increment Step
            obj.Step = obj.Step + 1;
        end
    end
end
