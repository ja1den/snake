classdef UI < handle
    % UI The user interface class for Snake.
    %   The UI class creates and holds the necessary UI components
    %   for a graphical game of snake.
    %
    %   See also: SnakeGame

    properties
        Figure
        TitleText
        MapPopupText
        MapPopup
        PlayButton
        Board
        PopupText
    end

    properties (SetAccess=immutable)
        MapNames (1, :) string = "Empty";
    end

    properties (Constant)
        FIGURE_SIZE = [688, 544];
    end

    methods
        function obj = UI(mapNames)
            % UI Create a new UI.
            %   I = UI(N) creates a new UI with map names N.

            if nargin > 0
                obj.MapNames = mapNames;
            end

            % Figure
            obj.Figure = figure();
            obj.Figure.WindowStyle = "normal";
            obj.Figure.Name = "Snake";
            obj.Figure.NumberTitle = false;
            obj.Figure.MenuBar = "none";
            obj.Figure.Position(3:4) = obj.FIGURE_SIZE;
            obj.Figure.Resize = false;

            movegui(obj.Figure, "center");

            % Title Text
            obj.TitleText = uicontrol(obj.Figure);
            obj.TitleText.Style = "text";
            obj.TitleText.String = "Snake!";
            obj.TitleText.FontSize = 18;
            obj.TitleText.FontWeight = "bold";
            obj.TitleText.HorizontalAlignment = "left";
            obj.TitleText.Position(1:2) = [16, obj.FIGURE_SIZE(2) - 48];
            obj.TitleText.Position(3:4) = [128, 32];

            % Map Popup Text
            obj.MapPopupText = uicontrol(obj.Figure);
            obj.MapPopupText.Style = "text";
            obj.MapPopupText.String = "Select a map:";
            obj.MapPopupText.FontSize = 8;
            obj.MapPopupText.HorizontalAlignment = "left";
            obj.MapPopupText.Position(1:2) = [16, ...
                obj.FIGURE_SIZE(2) - 80];
            obj.MapPopupText.Position(3:4) = [128, 16];

            % Map Popup
            obj.MapPopup = uicontrol(obj.Figure);
            obj.MapPopup.Style = "popupmenu";
            obj.MapPopup.String = mapNames;
            obj.MapPopup.Position(1:2) = [16, obj.FIGURE_SIZE(2) - 112];
            obj.MapPopup.Position(3:4) = [128, 32];

            % Play Button
            obj.PlayButton = uicontrol(obj.Figure);
            obj.PlayButton.Style = "pushbutton";
            obj.PlayButton.String = "Play!";
            obj.PlayButton.Position(1:2) = [16, obj.FIGURE_SIZE(2) - 160];
            obj.PlayButton.Position(3:4) = [128, 32];

            % Board
            obj.Board = axes(obj.Figure);
            obj.Board.Units = "pixels";
            obj.Board.Position(1:2) = [160, 16];
            obj.Board.Position(3:4)=  obj.FIGURE_SIZE - [176, 32];

            imshow(1, "Parent", obj.Board);

            % Popup Text
            obj.PopupText = uicontrol(obj.Figure);
            obj.PopupText.Style = "text";
            obj.PopupText.FontWeight = "bold";
            obj.PopupText.BackgroundColor = "#808080";
            obj.PopupText.ForegroundColor = "#FFFFFF";
            obj.PopupText.Visible = "off";
        end

        function obj = setPopupText(obj, message, size)
            % SETPOPUPTEXT Set the popup text.
            %   SETPOPUPTEXT(M, S) sets the popup text to the message M,
            %   with font size S.
            %
            %   Note: If the message M is empty, the popup is hidden.

            arguments
                obj (1, 1) UI
                message (1, 1) string = "";
                size (1, 1) double = 48;
            end

            % Empty?
            if strlength(message) == 0
                obj.PopupText.Visible = "off";
                return;
            end

            % Set Popup Text
            obj.PopupText.Visible = "on";
            obj.PopupText.String = message;
            obj.PopupText.FontSize = size;
            obj.PopupText.Position(1:2) = [160, obj.FIGURE_SIZE(2) / ...
                2 - obj.PopupText.Extent(4) / 2];
            obj.PopupText.Position(3:4) = [512, obj.PopupText.Extent(4)];
        end
    end
end
