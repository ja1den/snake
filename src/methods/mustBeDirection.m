function mustBeDirection(value)
    % MUSTBEDIRECTION Ensure a value is a Direction.
    %   MUSTBEDIRECTION(X) throws an error if X is not a Direction.
    %
    %   See also: Direction

    % Check against Directions
    isDirection = ...
        isequal(value, Direction.Up   ) || ...
        isequal(value, Direction.Down ) || ...
        isequal(value, Direction.Left ) || ...
        isequal(value, Direction.Right);

    % Throw Exception
    if ~isDirection
        throw(MException("SNAKE:validators:mustBeDirection", ...
            "Value must be Direction."));
    end
end
