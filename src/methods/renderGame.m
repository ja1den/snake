function image = renderGame(snakeGame)
    % RENDERGAME Render a SnakeGame as an image.
    %   I = RENDERGAME(G) creates 50-by-50-by-3 matrix containing the RGB
    %   image data for SnakeGame G.
    %
    %   Note: This method does not display or save the image.

    arguments
        snakeGame (1, 1) SnakeGame
    end

    % Preallocate Image
    image = ones(50, 50, 3, "uint8") * 255;

    % Iterate over Blocks
    for i = 1:size(snakeGame.Map.Blocks, 1)
        % Get Block
        block = snakeGame.Map.Blocks(i, :);

        % Render Block
        image(block(1):block(2), block(3):block(4), :) = 0;
    end

    % Iterate over Snakes
    for i = 1:length(snakeGame.Snakes)
        % Get Snake
        snake = snakeGame.Snakes(i);

        % Iterate over Cells
        for j = 1:size(snake.Cells, 1)
            % Get Cell
            cell = snake.Cells(j, :);

            % Render Cell
            if i == 1
                image(cell(1), cell(2), :) = [255, 0, 0];
            else
                image(cell(1), cell(2), :) = [0, 0, 255];
            end

            % Last Cell?
            if j == size(snake.Cells, 1) && j ~= 1
                % Just Grew?
                if snake.Step - 1 < 3 || mod(snake.Step - 3, 6) == 0
                    % Empty Channels
                    zeros = image(cell(1), cell(2), :) == 0;

                    % Hightlight Cell
                    image(cell(1), cell(2), zeros) = 128;
                end
            end
        end
    end

    % Iterate over Snakes
    for i = 1:length(snakeGame.Snakes)
        % Hit?
        if snakeGame.Losers(i)
            % Get Head
            head = snakeGame.Snakes(i).Cells(1, :);

            % Highlight Head
            image(head(1), head(2), :) = [0, 255, 0];
        end
    end
end
